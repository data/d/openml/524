# OpenML dataset: pbc

https://www.openml.org/d/524

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

------------------------------------------------------------------------
Primary Biliary Cirrhosis

The data set found in appendix D of Fleming and Harrington, Counting
Processes and Survival Analysis, Wiley, 1991.  The only differences are:
age is in days
status is coded as 0=censored, 1=censored due to liver tx, 2=death
the sex and stage variables are not missing for obs 313-418

Quoting from F&H.  "The following pages contain the data from the Mayo Clinic
trial in primary biliary cirrhosis (PBC) of the liver conducted between 1974
and 1984.  A description of the clinical background for the trial and the
covariates recorded here is in Chapter 0, especially Section 0.2.  A more
extended discussion can be found in Dickson, et al., Hepatology 10:1-7 (1989)
and in Markus, et al., N Eng J of Med 320:1709-13 (1989).
"A total of 424 PBC patients, referred to Mayo Clinic during that ten-year
interval, met eligibility criteria for the randomized placebo controlled
trial of the drug D-penicillamine.  The first 312 cases in the data set
participated in the randomized trial and contain largely complete data.  The
additional 112 cases did not participate in the clinical trial, but consented
to have basic measurements recorded and to be followed for survival.  Six of
those cases were lost to follow-up shortly after diagnosis, so the data here
are on an additional 106 cases as well as the 312 randomized participants.
Missing data items are denoted by `.'. "

Variables:
case number
number of days between registration and the earlier of death,
transplantion, or study analysis time in July, 1986
status
drug: 1= D-penicillamine, 2=placebo
age in days
sex: 0=male, 1=female
presence of asictes:       0=no 1=yes
presence of hepatomegaly   0=no 1=yes
presence of spiders        0=no 1=yes
presence of edema          0=no edema and no diuretic therapy for edema;
.5 = edema present without diuretics, or edema resolved by diuretics;
1 = edema despite diuretic therapy
serum bilirubin in mg/dl
serum cholesterol in mg/dl
albumin in gm/dl
urine copper in ug/day
alkaline phosphatase in U/liter
SGOT in U/ml
triglicerides in mg/dl
platelets per cubic ml / 1000
prothrombin time in seconds
histologic stage of disease


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: 3

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/524) of an [OpenML dataset](https://www.openml.org/d/524). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/524/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/524/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/524/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

